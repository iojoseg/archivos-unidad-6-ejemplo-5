import java.io.*;
import java.nio.file.*;


public class FlujodeDatos2{
  public static void main(String args[]){
  
  Path inputpath = Paths.get("entrada.txt");
  Path outputpath = Paths.get("salida.txt");
  
  InputStream input;
  OutputStream output;
  try{
  input = Files.newInputStream(inputpath, StandardOpenOption.READ);
  Files.newOutputStream(outputpath, StandardOpenOption.CREATE);
  output = Files.newOutputStream(outputpath, StandardOpenOption.WRITE);
  
  int i;
  byte[] data = new byte[1024];
  
  while((i = input.read(data)) != -1){
     output.write(data);
  }
  System.out.println("Se ha escrito en el archivo");
  input.close();
  output.close();
  }catch(IOException ex){
  ex.printStackTrace();
  }
  }
}