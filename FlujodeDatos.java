import java.io.*;
import java.nio.file.*;


public class FlujodeDatos{
  public static void main(String args[]){
  
  Path inputpath = Paths.get("entrada.txt");
  
  InputStream input;
  
  try{
  input = Files.newInputStream(inputpath, StandardOpenOption.READ);
  int i;
  while((i = input.read()) != -1){
     System.out.print((char)i);
  }
  
  input.close();
  
  }catch(IOException ex){
  ex.printStackTrace();
  }
  }
}